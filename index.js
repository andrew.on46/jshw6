const arr = [2, "hi", 23, true];
const type = "string";

const filterBy = (arr, type) => arr.filter((el) => typeof el !== type);

const clonedArr = filterBy(arr, type);

console.log(clonedArr);
